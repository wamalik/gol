1.To run the application, please build the war using following maven command 
  
 'mvn clean install'
   
2.Deploy the war file on web server and run the gameoflife.jsp file

e.g 'http://localhost:8080/ConwayGol/gameoflife.jsp'

3. Application will require the user to enter the number of generations/executions, please enter the digit value and click on 'play' button

4. Application will display number of  current state and view of cells grid after each execution

5. This is simple web application to show basic working of CONWAY Game of life, It is built using following technologies
      
  -  Java 
  -  Jersey (Rest API)
  -  JUnit, Mockito, Jersey Getty Mocking Framework 
  -  Angular JS  
  -  Maven