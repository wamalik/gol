package com.gameoflife.services.impl;

import com.gameoflife.model.Grid;

import java.util.List;

import com.gameoflife.model.Cell;
import com.gameoflife.model.GridState;
import com.gameoflife.services.RenderingService;

public class RenderingServiceImpl implements RenderingService{
	
	public GridState renderGridStateHtmlText(Grid grid, int num){
		
		GridState state = null;
	    if(grid != null){			
			state =	new GridState();
    	state.setNum(num);
           	
    	String currentView ="";
    	
        if(num == 0){
        	currentView ="<br> Default State<br>";
        }
        else {
    	currentView ="<br> State:  "+num+"<br>";
        }
       
    	Cell[][] cells = grid.getCells();
        
        
        String border = String.format("-%0" + 2*cells.length + "d-", 0).replace("0","-");
        currentView += border+"<br>";
        
        for (Cell[] row : cells) {
            //String r = "|";
        	String r="";
            for (Cell c : row) {
                r += c.getCurrentState() ? "*" + "&nbsp;" : "&nbsp;&nbsp;";
            }
            r += "";
            currentView += r+"<br>";
        }
        
        currentView += border;
        state.setView(currentView);
	    }
        
        return state;
        
	}
	
	@Override
	public String renderGrid(List<GridState> states) {
       StringBuilder gridHTMLText= new StringBuilder();
		
		for(int i=0;i<states.size();i++){
			gridHTMLText.append(states.get(i).getView() + "<br><br><br>");
		}
		return gridHTMLText.toString();
	}

}
