package com.gameoflife.services.impl;

import java.util.ArrayList;
import java.util.List;

import com.gameoflife.model.Grid;
import com.gameoflife.model.GridState;
import com.gameoflife.services.RenderingService;
import com.gameoflife.services.GenerationExecutionService;

public class GenerationExecutionServiceImpl implements GenerationExecutionService {

	private RenderingService displayService;
	
	public GenerationExecutionServiceImpl(){
		displayService = new RenderingServiceImpl();
	}
	
	@Override
public List<GridState> executeGenerations(int nbrOfGens) {
		List<GridState> gridStates = new ArrayList<GridState>();
		
		try{
		Grid grid = new Grid(10, 10, 0.3); 
		
		// add default state
		GridState defaultState = displayService.renderGridStateHtmlText(grid,0);
		gridStates.add(defaultState);
		
		//execute generations
		
		 for (int i = 1; i <= nbrOfGens; i++) {
			    grid.executeNextGeneration();
	        	GridState state = displayService.renderGridStateHtmlText(grid,i);
	        	gridStates.add(state);
	        }
		}catch(Exception e){
			// to do 
		}
	
	return gridStates;
}
}
