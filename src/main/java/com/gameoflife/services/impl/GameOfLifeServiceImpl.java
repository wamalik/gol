package com.gameoflife.services.impl;

import java.util.List;

import com.gameoflife.model.GridState;
import com.gameoflife.services.GameOfLifeService;
import com.gameoflife.services.GenerationExecutionService;
import com.gameoflife.services.RenderingService;

public class GameOfLifeServiceImpl implements GameOfLifeService {
	
	
	private GenerationExecutionService generationExecutionService;
	RenderingService renderingService;
	
	public GameOfLifeServiceImpl() {
		generationExecutionService = new GenerationExecutionServiceImpl();
		renderingService = new RenderingServiceImpl();
	}

	@Override
	public String playGame(int numOfGenerations) {
		List<GridState> states = generationExecutionService.executeGenerations(numOfGenerations);
		return renderingService.renderGrid(states);
	}
	
	
	
	

}
