package com.gameoflife.services;

import java.util.List;

import com.gameoflife.model.Grid;
import com.gameoflife.model.GridState;

public interface RenderingService {
	
    public GridState renderGridStateHtmlText(Grid board, int num); 
    public String renderGrid(List<GridState> states);
}
