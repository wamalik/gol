package com.gameoflife.services;
import com.gameoflife.model.GridState;

import java.util.List;

import com.gameoflife.model.Grid;


public interface GenerationExecutionService {
	
	public List<GridState> executeGenerations(int nbrOfGens);

}
