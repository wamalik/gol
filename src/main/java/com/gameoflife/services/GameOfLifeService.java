package com.gameoflife.services;

import java.util.List;
import com.gameoflife.model.GridState;

public interface GameOfLifeService {
	public String playGame(int numOfGenerations);
	

}
