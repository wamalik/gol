package com.gameoflife.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.ArrayList;
import java.util.List;

import com.gameoflife.model.GridState;
import com.gameoflife.services.GameOfLifeService;

import com.gameoflife.services.impl.*;;

@Path("gol")
public class ConwayGolRestService {

	@GET
	@Path("/{generations}")
	@Produces(MediaType.TEXT_HTML)
	public String playGolGame(@PathParam("generations") String numOfGenerations) {
	
		String htmlContents = "Invalid Input detected";
		
		GameOfLifeService gameOfLifeService = new GameOfLifeServiceImpl();
		try{
			htmlContents = gameOfLifeService.playGame(Integer.parseInt(numOfGenerations));
			
		}catch(Exception e){
			// to do
		}
		return htmlContents;
	}

}
