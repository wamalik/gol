package com.gameoflife.model;

public class Grid {
    private Cell[][] cells;
    private int height=3; 
    private int width=3;

    public Grid(Cell[][] cells) {
        this.cells = cells;
        height = width = cells.length;
    }

    /**
     * @param height
     * @param width
     * @param p probability that Cell is alive at start
     */
    public Grid(int height, int width, double p) {
        this.height=height;
        this.width = width;
        cells = new Cell[height][width];
        
        for (int h=0; h<cells.length; h++){
            for (int w=0; w<cells[h].length; w++){
                cells[h][w] = new Cell();
                if (Math.random()<=p){
                    cells[h][w].setNewState(true);
                    cells[h][w].updateState();
                }
            }
        }
    }

    public Cell[][] getCells() {
        return cells;
    }
    
    public int getSize() {
        return width;
    }

    public int getNeighboursCount(int row, int col) {
        int sum=0;
        if (row != 0 && col != 0){    //1
            if(isCellAlive(row-1,col-1)){
                sum++;
            }
        }
        
        if (row != 0){
            if(isCellAlive(row-1,col)){ //2
            sum++;
            }
        }
        
        if (row != 0 && col != width-1){//3
            if(isCellAlive(row-1,col+1)){
                sum++;
            }
        }
        if (col != 0){
            if(isCellAlive(row,col-1)){ //4
            sum++;
            }
        }
        //self
        if (col != width-1){
            if(isCellAlive(row,col+1)){ //6
                sum++;
            }
        }

        if (row != height-1 && col != 0){
            if(isCellAlive(row+1,col-1)){ //7
                sum++;
            }
        }

        if (row != height-1){
            if(isCellAlive(row+1,col)){ //8
            sum++;
            }
        }

        if (row != height-1 && col != width-1){
            if(isCellAlive(row+1,col+1)){ //9
                sum++;
            }
        }

        return sum;
    }

    public boolean isCellAlive(int row, int col) {
        return cells[row][col].getCurrentState();
    }

    public void executeNextGeneration() {
        configureNewStateToCells();
        updateCellState();
    }

    /**
     * Assigns new state to individual Cells 
     * according to GoF rules
     */
    private void configureNewStateToCells() {
        for (int h=0; h<cells.length; h++){
            for (int w=0; w<cells[h].length; w++){
                int nr = getNeighboursCount(h,w);                
                if (nr < 2) { cells[h][w].setNewState(false);}  //underpop
                else if (nr > 3) { cells[h][w].setNewState(false);} //overcrowd
                else if (nr == 3) { cells[h][w].setNewState(true);} //born
                else if (nr == 2) { cells[h][w].setNewState(cells[h][w].getCurrentState());} // stay same
            }
        }
    }

    /**
     * Updates Cell state based on newState
     */
    private void updateCellState() {
        for (int h=0; h<cells.length; h++){
            for (int w=0; w<cells[h].length; w++){
                cells[h][w].updateState();
            }
        }
    }
    
    
}
