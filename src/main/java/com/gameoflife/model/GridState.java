
package com.gameoflife.model;

public class GridState {
	private int num;
	private String view;
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public String getView() {
		return view;
	}
	public void setView(String view) {
		this.view = view;
	}
	

}
