package com.gameoflife.rest;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.Test;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;


public class ConwayGolRestServiceTest extends JerseyTest  {
	
	  @Override
	    public Application configure() {
	        enable(TestProperties.LOG_TRAFFIC);
	        enable(TestProperties.DUMP_ENTITY);
	        return new ResourceConfig(ConwayGolRestService.class);
	    }
	
	 @Test
	    public void shouldReturnSucess() {
	        Response output = target("/gol/1").request().get();
	        assertEquals("should return status 200", 200, output.getStatus());
	        assertNotNull("Should return String", output.getEntity());
	    }
	 
	  @Test
	    public void shoudlNotFail_EvenDoesNotHaveDigit(){
	        Response output = target("/gol/no-id-digit").request().get();
	        assertEquals("Should return status 200", 200, output.getStatus());
	    }
	
	
}
