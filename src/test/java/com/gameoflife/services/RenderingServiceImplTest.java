package com.gameoflife.services;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.gameoflife.model.Grid;
import com.gameoflife.model.GridState;
import com.gameoflife.services.impl.RenderingServiceImpl;
import com.gameoflife.services.RenderingService;

import static org.mockito.MockitoAnnotations.initMocks;import org.hamcrest.core.IsNot;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.hamcrest.CoreMatchers.equalTo;


public class RenderingServiceImplTest {
	
	
	private Grid grid;

	private RenderingService displayService;
	
	@Before
	public void setUp(){
		 initMocks(this);
		 displayService = new RenderingServiceImpl();
		 grid = new Grid(10, 10, 0.3);
		
	}
	
	@After
	public void tearDown(){
		
		displayService = null;
	}
	
	
	@Test
	public void shouldReturnInValidStateGivenInvalidGridAndStateNum(){
		//given 
		int stateNum = 0;
		Grid newGrid = null;
		
		//when
		GridState state = displayService.renderGridStateHtmlText(newGrid, stateNum);
		
		//then
		assertThat(state, is(nullValue()));
	}
	
	@Test
	public void shouldReturnValidStateGivenValidGridAndStateNum(){
		//given 
		int stateNum = 1;
		Grid newGrid = new Grid(10, 10, 0.3);
		
		//when
		GridState state = displayService.renderGridStateHtmlText(newGrid, stateNum);
		
		//then
		assertThat(state.getView(), is(notNullValue()));
		assertThat(state.getNum(), equalTo(stateNum));
	}
	
	
	
  
	
	
	

}
