package com.gameoflife.model;


import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.gameoflife.model.Cell;


public class CellTest {

    @Test
    public void shouldUpdateState() {
        Cell c = new Cell();

        c.setNewState(true);
        c.updateState();
        assertEquals(true, c.getCurrentState());
    }

   

}
